<?php

namespace Drupal\Tests\sqlite_test_cache\Kernel;

use Drupal\KernelTests\KernelTestBase;

/**
 * Base class for sqlite cached kernel tests.
 */
abstract class SqliteCachedKernelTestBase extends KernelTestBase {

  /**
   * Database setup tasks.
   *
   * These are ran only once per whole test suite run.
   */
  protected function setUpDatabase(): void {}

  /**
   * Class setup tasks.
   *
   * These are ran before every test method. Assigning class properties needs
   * to be done here.
   */
  protected function setUpClass(): void {}

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();

    [$lastAvailableClassCache, $missingClassCaches] = $this->getCacheInfo();

    if ($lastAvailableClassCache) {
      $this->restoreCache($lastAvailableClassCache);
      $this->callSetUpClassOnAncestorsAndSelf($lastAvailableClassCache);
    }

    foreach ($missingClassCaches as $class) {
      $setUpDatabaseMethod = $class->getMethod('setUpDatabase');

      // Only call each setup once. If the parent doesn't implement it
      // themselves don't call the inherited method.
      if ($setUpDatabaseMethod->getDeclaringClass()->getName() == $class->getName()) {
        $class->getMethod('setUpDatabase')->invoke($this);
      }

      $this->saveCache($class);
      $this->callSetUpClassOnAncestorsAndSelf($class, $lastAvailableClassCache);
      $lastAvailableClassCache = $class;
    }

    /** @var \Drupal\Core\Entity\EntityFieldManager */
    $entityFieldManager = $this->container->get('entity_field.manager');
    $entityFieldManager->clearCachedFieldDefinitions();

    /** @var \Drupal\Core\KeyValueStore\KeyValueMemoryFactory $keyValueMemory */
    $keyValueMemory = $this->container->get('keyvalue');
    $refObject = new \ReflectionObject($keyValueMemory);
    $refProperty = $refObject->getProperty('collections');
    $refProperty->setAccessible(TRUE);
    $refProperty->setValue($keyValueMemory, []);
  }

  /**
   * Returns the cache status of parent classes.
   */
  protected function getCacheInfo() {
    $lastAvailableClassCache = NULL;
    $missingClassCaches = [];
    $currentClass = new \ReflectionClass($this);
    while ($currentClass->getName() != self::class) {
      $cacheExists = file_exists($this->getDbCachePath($currentClass));
      if (!$cacheExists) {
        $missingClassCaches[] = $currentClass;
      }
      elseif (!$lastAvailableClassCache) {
        $lastAvailableClassCache = $currentClass;
      }
      $currentClass = $currentClass->getParentClass();
    }
    return [$lastAvailableClassCache, array_reverse($missingClassCaches)];
  }

  /**
   * Restores the cached database.
   */
  protected function restoreCache($class) {
    $cachePatch = $this->getDbCachePath($class);
    copy($cachePatch, $this->getDatabasePath());
  }

  /**
   * Save the cache.
   */
  protected function saveCache($class) {
    if (!$this->useCache()) {
      return;
    }
    $cachePatch = $this->getDbCachePath($class);
    copy($this->getDatabasePath(), $cachePatch);
  }

  /**
   * Calls the setUpClass method on the class and its anscestors up to $until.
   */
  protected function callSetUpClassOnAncestorsAndSelf(\ReflectionClass $class, ?\ReflectionClass $until = NULL) {
    $methods = [];

    $lastAnscestors = [self::class];

    if ($until) {
      $lastAnscestors[] = $until->getName();
    }

    while (!in_array($class->getName(), $lastAnscestors)) {
      $method = $class->getMethod('setUpClass');
      while ($method->getDeclaringClass()->getName() != $class->getName()) {
        $class = $class->getParentClass();
      }

      $methods[] = $method;

      $class = $class->getParentClass();
    }

    foreach (array_reverse($methods) as $method) {
      $method->invoke($this);
    }
  }

  /**
   * Returns the path to the database file.
   */
  protected function getDatabasePath() {
    $connection = $this->container->get('database');
    $this->assertInstanceOf('\Drupal\sqlite\Driver\Database\sqlite\Connection', $connection, 'SqliteCachedKernelTestBase can only be used with sqlite database.');
    $options = $connection->getConnectionOptions();
    $prefix = current($connection->getAttachedDatabases());
    return $options['database'] . '-' . $prefix;
  }

  /**
   * Return the patch to cached database for the given class.
   */
  protected function getDbCachePath(\ReflectionClass $class) {
    return dirname($this->getDatabasePath()) . '/cache-' . $class->getShortName();
  }

  /**
   * Tells if the sqlite test cache should be used.
   */
  protected function useCache() {
    $enabled = TRUE;
    \Drupal::moduleHandler()->alter('sqlite_test_cache_enabled', $enabled);
    return $enabled;
  }

}
